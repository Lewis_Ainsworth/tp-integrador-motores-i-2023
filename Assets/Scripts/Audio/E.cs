using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E : MonoBehaviour
{
    public AudioSource Objeto;
    public AudioClip PickUp_Audio;


    void OnTriggerStay(Collider other)
    {
        if (Input.GetKey(KeyCode.E))
        {
            AudioSource.PlayClipAtPoint(PickUp_Audio, gameObject.transform.position);
            Destroy(gameObject);
        }
        
    }
}
