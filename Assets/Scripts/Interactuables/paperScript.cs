using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paperScript : MonoBehaviour
{
    public GameObject paper, inticon, particles;
    public AudioSource pickup;


    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("MainCamera"))
        {
            inticon.SetActive(true);
            if (Input.GetKey(KeyCode.E))
            {
                paper.SetActive(false);
                inticon.SetActive(false);
                particles.SetActive(false);
                pickup.Play();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MainCamera"))
        {
            inticon.SetActive(false);
        }
    }
}