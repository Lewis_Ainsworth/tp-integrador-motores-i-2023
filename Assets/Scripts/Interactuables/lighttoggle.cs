using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lighttoggle : MonoBehaviour
{
    public Light linterna;
    private bool linternaEncendida = false;

    void Update()
    {
        if (Input.GetKey(KeyCode.F))
        {
            ToggleLinterna();
        }
    }

    void ToggleLinterna()
    {
        linternaEncendida = !linternaEncendida;

        linterna.enabled = linternaEncendida;
    }
}
