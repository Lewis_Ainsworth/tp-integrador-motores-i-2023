using UnityEngine;

public class monsterChase : MonoBehaviour
{
    public Rigidbody monsRigid;
    public Transform monsTrans, playTrans;
    public int monSpeed;

    void FixedUpdate()
    {
        monsRigid.velocity = monsTrans.forward * monSpeed * Time.fixedDeltaTime;
        monsTrans.LookAt(playTrans.position);
    }
}
