using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monsAppear : MonoBehaviour
{
    public GameObject JumpScareImg;
    public GameObject JumpScareSpray;
    public AudioSource JumpScareAudio;

    void Start() 
    {
        JumpScareImg.SetActive(false);
        JumpScareSpray.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(EnableImg());
            JumpScareSpray.SetActive(true);
            StartCoroutine(CerrarJuego());
        }
    }

    IEnumerator EnableImg() 
    {
        yield return new WaitForSeconds(7);
        JumpScareImg.SetActive(true);
        JumpScareAudio.Play();
    }

    IEnumerator CerrarJuego()
    {
        yield return new WaitForSeconds(9);
        Application.Quit();
    }
}