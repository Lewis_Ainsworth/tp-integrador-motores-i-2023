using UnityEngine;

public class ControlEnemigo : MonoBehaviour 
{ 
    public GameObject explosion; 
    


    public void Explotar(int segundos) 
    { 
        Invoke("MostrarExplosion", segundos); 
        Destroy(gameObject, segundos); 
    } 
    
    
    public void MostrarExplosion() 
    { 
        GameObject particulas = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject; 
        Destroy(particulas, 2); 
    } 
}