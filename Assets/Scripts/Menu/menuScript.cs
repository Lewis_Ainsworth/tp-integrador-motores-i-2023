using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuScript : MonoBehaviour
{

    public GameObject options, musicmenu;


    // Start is called before the first frame update
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("Menu");
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    public void Play()
    {
        SceneManager.LoadScene("Game");
        SceneManager.UnloadScene("Menu");
        GestorDeAudio.instancia.PausarSonido("Menu");
    }


    public void OptionsOn() 
    { 
        options.SetActive(true);
    }


    public void OptionsOff()
    {
        options.SetActive(false);
    }


    public void Quit() 
    {
        Application.Quit();
    }
}
