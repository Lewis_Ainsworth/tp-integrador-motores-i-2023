using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuPausa : MonoBehaviour
{
    public GameObject ObjetoMenuPausa, options;
    private bool isPaused = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            isPaused = !isPaused;
            ObjetoMenuPausa.SetActive(true);
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;

            HandlePause();
        }
       
    }

    void HandlePause()
    {
        Time.timeScale = isPaused ? 0 : 1;
    }


    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
        SceneManager.UnloadScene("Game");
        GestorDeAudio.instancia.PausarSonido("Game");
        GestorDeAudio.instancia.ReproducirSonido("Menu");
    }


    public void Continue()
    {
        ObjetoMenuPausa.SetActive(false);
        Time.timeScale = 1;
    }


    public void Quit()
    {
        Application.Quit();
    }

    public void OptionsOn()
    {
        options.SetActive(true);
    }


    public void OptionsOff()
    {
        options.SetActive(false);
    }
}
