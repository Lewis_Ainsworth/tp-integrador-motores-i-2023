using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mear : MonoBehaviour
{
    public GameObject peeParticles;
    private bool isPeeing = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            StartPeeing();
        }

        if (Input.GetKeyUp(KeyCode.C))
        {
            StopPeeing();
        }
    }

    void StartPeeing()
    {
        peeParticles.SetActive(true);
        isPeeing = true;
    }

    void StopPeeing()
    {
        peeParticles.SetActive(false);
        isPeeing = false;
    }
}
