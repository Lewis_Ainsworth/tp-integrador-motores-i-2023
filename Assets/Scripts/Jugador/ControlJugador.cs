using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour 
{

    private void Start() 
    { 
        GestorDeAudio.instancia.ReproducirSonido("Musica");
        Cursor.lockState = CursorLockMode.Locked;
    } 
    
    void Update() 
    { 
        if (Input.GetKey(KeyCode.Space)) 
        { 
            GestorDeAudio.instancia.ReproducirSonido("Moneda"); 
        }

    }
}